"use strict";
const assetsPath = "./mimer-assets-pre/";

export class InOrder extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "InOrder" });
    this.gameOn = true;
    this.score = 0;
    this.scoreMsg = "Score: ";
    this.scoreText;
    this.previus = -1;
  }

  preload() {
    this.load.spritesheet(
      "numbers",
      assetsPath + "numbers/number-block-128x128-10x5.png",
      { frameWidth: 128, frameHeight: 128 }
    );
  }

  create() {
    this.scoreText = this.add.text(10, 10, this.scoreMsg + this.score, {
      fontSize: "32px",
      fill: "#FFF"
    });
    this.input.on(
      "gameobjectup",
      function(pointer, gameObject) {
        gameObject.emit("clicked", gameObject);
      },
      this
    );

    for (let i = 0; i < 10; i++) {
      let point = this.randomPosistion();
      let my_number = this.add
        .image(point.x, point.y, "numbers", i)
        .setScale(0.5);
      my_number.setInteractive();
      my_number.on("clicked", this.clickNumberHandler, this);
      my_number.myNumber = i;

      var myVar = setInterval(myTimer, 1);

      function myTimer() {
        my_number.rotation += 0.01;
      }
    }
  }

  update() {}

  randomPosistion() {
    let point = {};
    point.x = Phaser.Math.Between(64, 736);
    point.y = Phaser.Math.Between(64, 500);

    return point;
  }

  clickNumberHandler(number) {
    console.log(this.previus);
    console.log(number.myNumber);
    if (this.previus + 1 === number.myNumber) {
      this.previus++;
      this.score++;
      this.scoreText.setText(this.scoreMsg + this.score);
      number.off("clicked", this.clickGoodMushroomHandler);
      number.input.enabled = false;
      number.setVisible(false);
      if (this.previus === 9) {
        this.add.text(25, 200, "Winner", { fontSize: "128px", fill: "#0F0" });
      }
    } else {
      this.add.text(25, 200, "Game Over", { fontSize: "128px", fill: "#F00" });
    }
  }
}
